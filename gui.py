import tkinter as tk
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from ttkbootstrap.dialogs import Messagebox

# Classe pour gérer l'interface utilisateur tkinter
class UserForm:
    def __init__(self, user_database):
            self.user_database = user_database

            self.window = ttk.Window(themename="morph")
            self.window.title("Gestion utilisateur")
            self.window.geometry("600x400")
            # Centrer la fenêtre sur l'écran
            screen_width = self.window.winfo_screenwidth()
            screen_height = self.window.winfo_screenheight()
            window_width = 600
            window_height = 400
            x = (screen_width - window_width) // 2
            y = (screen_height - window_height) // 2
            self.window.geometry(f"{window_width}x{window_height}+{x}+{y}")
        
            self.table = ttk.Treeview(self.window, columns=("ID", "Nom", "Âge"), show="headings")
            self.table.heading("ID", text="ID")
            self.table.heading("Nom", text="Nom")
            self.table.heading("Âge", text="Âge")
            self.table.pack(fill="both", expand=True)
            
            # Créer un LabelFrame pour les boutons
            button_frame = ttk.LabelFrame(self.window, text="Actions", padding=10)
            button_frame.pack(pady=10)

            self.button_add = ttk.Button(button_frame, text="Ajouter", command=self.open_add_user_form, style=SUCCESS)
            self.button_add.pack(side=tk.LEFT, padx=5)

            self.button_modify = ttk.Button(button_frame, text="Modifier", command=self.open_add_user_form, style=WARNING)
            self.button_modify.pack(side=tk.LEFT, padx=5)

            self.button_delete = ttk.Button(button_frame, text="Supprimer", command=self.delete_user, style=DANGER)
            self.button_delete.pack(side=tk.LEFT, padx=5)

            self.populate_data()
            self.window.mainloop()

    def populate_data(self):
        users = self.user_database.get_all_users()
        self.table.delete(*self.table.get_children())
        for user in users:
            self.table.insert("", "end", values=(user.id, user.name, user.age))
    
    def open_add_user_form(self):
        add_user_form = AddUserForm(self.user_database, self.populate_data, self.window)
        
    def delete_user(self):
        if selected_item := self.table.selection():
            user_id = self.table.item(selected_item)['values'][0]
            self.user_database.delete_user(user_id)
            self.populate_data()
        else:
            md = Messagebox()
            md.show_warning(title="Erreur", message="Veuillez sélectionner un utilisateur.", parent=self.window)

# Classe pour ajouter un utilisateur 
class AddUserForm:
    def __init__(self, user_database, callback, parent):
        self.user_database = user_database
        self.callback = callback
        self.parent = parent

        self.form_window = ttk.Toplevel(parent)
        self.form_window.title("Ajouter un utilisateur")
        self.form_window.geometry(
            f"+{parent.winfo_rootx() + 50}+{parent.winfo_rooty() + 50}"
        )

        self.label_name = ttk.Label(self.form_window, text="Nom :")
        self.label_name.pack()

        self.entry_name = ttk.Entry(self.form_window)
        self.entry_name.pack()

        self.label_age = ttk.Label(self.form_window, text="Âge :")
        self.label_age.pack()

        self.entry_age = ttk.Entry(self.form_window)
        self.entry_age.pack()

        # Créer un cadre pour aligner les boutons avec une marge
        button_frame = ttk.Frame(self.form_window)
        button_frame.pack(pady=10)  # Ajouter de la marge autour du cadre

        self.button_validate = ttk.Button(button_frame, text="Valider", command=self.add_user)
        self.button_validate.pack(side=tk.LEFT, padx=5)  # Aligner à gauche avec un espacement horizontal

        self.button_cancel = ttk.Button(button_frame, text="Annuler", command=self.form_window.destroy)
        self.button_cancel.pack(side=tk.LEFT, padx=5)  # Aligner à gauche avec un espacement horizontal

        self.form_window.mainloop()

    def add_user(self):
        name = self.entry_name.get()
        age = int(self.entry_age.get())
        self.user_database.add_user(name, age)
        self.callback()
        self.form_window.destroy()  
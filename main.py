import tkinter as tk
from database_handler import UserDatabase
from gui import UserForm


if __name__ == "__main__":
    # Création de l'instance de la classe UserDatabase
    user_db = UserDatabase()
    # Création de l'instance de la classe UserForm et passe la référence à UserDatabase
    user_form = UserForm(user_db)
    # Lancement de l'application tkinter
    user_form.window.mainloop()

greenlet==2.0.2
Pillow==9.5.0
SQLAlchemy==2.0.13
tabulate==0.9.0
ttkbootstrap==1.10.1
typing_extensions==4.5.0

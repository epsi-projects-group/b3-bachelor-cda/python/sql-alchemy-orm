from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Configuration de la base de données SQLAlchemy
db_url = 'sqlite:///./test.db'
engine = create_engine(db_url, echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()


class UserDatabase:
    def __init__(self):
        self.Session = Session

    def add_user(self, name, age):
        session = self.Session()
        user = User(name=name, age=age)
        session.add(user)
        session.commit()
        session.close()
        
    def delete_user(self, user_id):
        session = self.Session()
        if user := session.query(User).get(user_id):
            session.delete(user)
            session.commit()
        session.close()
        
    def get_all_users(self):
        session = self.Session()
        users = session.query(User).all()
        session.close()
        return users


# Définition de la classe User SQLAlchemy
class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)